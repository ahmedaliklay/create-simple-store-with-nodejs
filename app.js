const express = require('express'); // call express

const path = require('path'); //call directory

// session
const session = require('express-session');
const SessionStore = require('connect-mongodb-session')(session);
//End session
const homeRouter = require('./routes/home.route');//call with index page and product operation

const ProductRoute = require('./routes/product.route');
const AuthRoute = require('./routes/auth.route');
const cartRouter = require('./routes/cart.route');

const AddProduct = require('./routes/admin-route');
// flash session
const flash = require('connect-flash');
// End flash session
const app = express();

app.use(express.static(path.join(__dirname,'assets')));
app.use(express.static(path.join(__dirname,'images')));
//app.use(express.static(__dirname + '/assets'));
app.use(flash());
const STORE = new SessionStore({
    //db_url : 'mongodb://localhost:27017/online-shop',
    db_url: "mongodb+srv://AhmedAliKlay:Ahmed2020Klay@cluster0.yz8jm.mongodb.net/online-shop?retryWrites=true&w=majority",
    collection:'sessions'
});

app.use(session({
    secret: 'this is my secret to hash express ....',
    saveUninitialized: false,
    store: STORE
    // cookie:{ maxAge: 1*60*60*100
    //expires :new date}
}));
app.set('view engine','ejs');

app.set('views','views')
//!!!! !!!! !!!! !!!! !!!! !!!! !!!! !!!!
// app.get('/',function(req,res,next){
//     res.send('hellow projects');
// });

// app.get('/',function(req,res,next){
//     res.render('index');
// });
//!!!! !!!! !!!! !!!! !!!! !!!! !!!! !!!!

app.use('/',homeRouter);

app.use('/product',ProductRoute);

app.use('/',AuthRoute);

app.use('/cart',cartRouter);

app.use('/admin',AddProduct);

app.use('/error',(req,res,next)=>{
    res.status(500);
    res.render('error.ejs',{
        isUser: req.session.userId
    });
});

// mudlower page not found
app.use((req,res,next) => {
    res.status(404);
    res.render('not-found',{
        isUser: req.session.userId,
        pageTitle : 'page not found'
    });
}); 
//app.use('/product',ProductRoute);


//!!!! !!!! !!!! !!!! !!!! !!!! !!!! !!!!
const port = process.env.PORT || 3000;
app.listen(port,function(err){
    console.log('errores : ' + err);
    console.log('server listen in port 3000');
});