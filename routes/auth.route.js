const router = require('express').Router();

const bodyParser = require('body-parser');

const check = require('express-validator').check;

const authGuard = require('./guards/auth.guard');

const authController = require('../controllers/auth.controller');


router.get('/signup',authGuard.notAuth,authController.getSignup);
/*
create validation [npm install --save express-validator]
*/ 
router.post('/signup',authGuard.notAuth,
    bodyParser.urlencoded({ extended:true }),
    //.not().isEmpty
    //confirmPassword  username email  password
    check('username')
    .not().isEmpty().withMessage('User Name is Require'),
    check('email')
    .not().isEmpty().withMessage('Email is require')
    .isEmail().withMessage('invalid Formate'),
    check('password')
    .not().isEmpty().withMessage('password is Require')
    .isLength({min: 6}),
    check('confirmPassword').custom((value,{ req }) => {
        if(value === req.body.password) return true;
        else throw 'password don"t equal';
        
    }),
    authController.postSignup
);

router.get('/login',authGuard.notAuth,authController.getLogin);

router.post('/login',authGuard.notAuth,
    bodyParser.urlencoded({ extended:true }),
    //confirmPassword  username email  password
    check('email')
    .not().isEmpty().withMessage('Email is require')
    .isEmail().withMessage('invalid Formate'),
    check('password')
    .not().isEmpty().withMessage('password is Require')
    .isLength({min: 6}),
    authController.postLogin
);

router.all('/logout',authGuard.isAuth,authController.logout);
module.exports = router;