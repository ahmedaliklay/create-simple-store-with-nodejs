const router = require('express').Router(); // call level Router

const authGuard = require('./guards/auth.guard'); 

const homeController = require('../controllers/home.controller'); //call page controller
//authGuard.isAuth,
router.get('/',homeController.getHome);



module.exports = router;