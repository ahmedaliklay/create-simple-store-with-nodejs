const express = require('express');
const router = express.Router();

const check = require('express-validator').check;
const multer = require('multer');

const adminController = require('../controllers/admin.controller');
const authGuard = require('./guards/auth.guard');


router.get('/add',adminController.getAdd);

router.post('/add',multer({
    //dest: 'images'
    storage:multer.diskStorage({
        destination: (req,file,cb)=>{
            cb(null,'images')
        },
        filename: (req,file,cb)=>{
            cb(null, Date.now() + '-' + file.originalname)
        }
    })
}).single('image'),
    check('image').custom((value,{req}) => {
        if(req.file) return true;
        else throw 'image is require';
    }),
    adminController.postAdd);

module.exports = router;