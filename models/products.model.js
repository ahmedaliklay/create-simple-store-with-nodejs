const mongoose = require('mongoose');

//const db_url = 'mongodb://localhost:27017/online-shop';
const db_url = "mongodb+srv://AhmedAliKlay:Ahmed2020Klay@cluster0.yz8jm.mongodb.net/online-shop?retryWrites=true&w=majority";
var Schema = mongoose.Schema;

const productSchema =  new Schema({
    name: String,
    price: Number,
    image: String,
    description: String,
    categor: String,
});

const Product = mongoose.model('product', productSchema);

exports.getAllProducts = () =>{
    //connect to DB
    //get products
    //disconnect
    return new Promise(function(resolve,reject){
        mongoose.connect(db_url).then(() => {

            return Product.find({});

        }).then((products) => {
            mongoose.disconnect();
            resolve(products);
        }).catch(err => reject(err));
    });

}
////////////////////////
exports.getProductsByCategory = (categor) =>{
    //connect to DB
    //get products
    //disconnect
    console.log(categor);
    return new Promise(function(resolve,reject){
        mongoose.connect(db_url).then(() => {
            return Product.find({categor: categor});
        }).then((products) => {
            mongoose.disconnect();
            resolve(products);
        }).catch(err => reject(err));
    });
}
////////////////////////
exports.getProductById = (id) =>{
    //connect to DB
    //get products
    //disconnect
    console.log(id);
    return new Promise(function(resolve,reject){
        mongoose.connect(db_url).then(() => {
            return Product.findById(id);
        }).then(function(product){
            mongoose.disconnect();
            resolve(product);
        }).catch(err => reject(err));
    });

}

////////////////////////
exports.getFirstProduct = function(){
    return new Promise(function(resolve,reject){
        mongoose.connect(db_url).then(() => {
            return Product.findOne({});
        }).then(function(product){
            mongoose.disconnect();
            resolve(product);
        }).catch(err => reject(err));
    });
}
////////////////////////

exports.addNewProduct = function(data){
    return new Promise(function(resolve,reject){
        //return reject('error');
        mongoose.connect(db_url).then(() => {
            let product = new Product(data);
            return product.save();
        }).then(()=>{
            mongoose.disconnect();
            resolve();
        }).then((err) => {
            mongoose.disconnect();
            reject(err);
        }); 
    });

}