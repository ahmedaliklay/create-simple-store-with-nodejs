const mongoose = require('mongoose');

//const db_url = 'mongodb://localhost:27017/online-shop';
const db_url = "mongodb+srv://AhmedAliKlay:Ahmed2020Klay@cluster0.yz8jm.mongodb.net/online-shop?retryWrites=true&w=majority";
const Schema = mongoose.Schema;

const cartSchema = mongoose.Schema({
    name: String,
    price: Number,
    amount: Number,
    userId: String,
    productId: String,
    timesTamp: Number
});

const CartItem = mongoose.model("cart", cartSchema); //carts

exports.addNewItem = function(data){
    return new Promise(function(resolve,reject){
        mongoose.connect(db_url).then(function(){
            let item = new CartItem(data);
            return item.save();
        }).then(function(){
            mongoose.disconnect();
            resolve();
        }).then(function(err){
            mongoose.disconnect();
            reject(err);
        });
    });
}

exports.getItemByUser = function(userId){
    //connect to DB //get products  //disconnect
    
    return new Promise(function(resolve,reject){

        mongoose.connect(db_url).then(function() {
            
            return CartItem.find({userId: userId},{},{sort:{ timesTamp:1 }});//ترتيب تصاعدي = 1

        }).then( function(items) {

            mongoose.disconnect();
            resolve(items);

        }).catch(function(err){
            mongoose.disconnect();
            reject(err);

        });

    });

}

exports.editItem = function(id,nowData){
    return new Promise(function(resolve,reject){

        mongoose.connect(db_url).then(() => {
            console.log(id+ '$$$$$$$$$');
            return CartItem.updateOne({_id: id},nowData);

        }).then( function(items) {

            mongoose.disconnect();
            resolve(items);

        }).catch(function(err){
            mongoose.disconnect();
            reject(err);

        });

    });
}

exports.deleteItem = function(cartId){
    //console.log(id);
    return new Promise(function(resolve,reject){

        mongoose.connect(db_url).then(function() {
            
            return CartItem.findByIdAndDelete(cartId);

        }).then( function(items) {

            mongoose.disconnect();
            resolve(items);

        }).catch(function(err){
            mongoose.disconnect();
            reject(err);

        });

    });
}
