const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

//const db_url = 'mongodb://localhost:27017/online-shop';
const db_url = "mongodb+srv://AhmedAliKlay:Ahmed2020Klay@cluster0.yz8jm.mongodb.net/online-shop?retryWrites=true&w=majority";
var Schema = mongoose.Schema;

const userSchema =  new Schema({
    username: String,
    email: String,
    password: String
});

const User = mongoose.model('user', userSchema); //users table

exports.createNewUser = function(username, email, password){

//check email exists //yes errors //no create new account

return new Promise( function(resolve,reject) {
    mongoose.connect(db_url,{ useNewUrlParser: true }).then( function() {
        return User.findOne({email:email});
    }).then(function(user){
        if(user){
            mongoose.disconnect();
            reject('email is used!');
        }else{
            // crypt => npm install --save bcrypt
            return bcrypt.hash(password, 10);
        }
    }).then(function(hashedPassword){
        let user = new User({
            username: username,
            email: email,
            password: hashedPassword
        });
        return user.save();
    }).then(function(){
        resolve('user created');
        mongoose.disconnect();
    }).catch( function(err){
        mongoose.disconnect();
        reject(err);
        });
});// end new Promise


}

exports.login = function(email,password){
    //check => for email
    //yes   => check for password
    //no    => error
    //yes   =>
    return new Promise(function(resolve,reject){
        mongoose.connect(db_url).then(function(){
            User.findOne({ email: email }).then(function(user){
                if(!user){
                    mongoose.disconnect();
                    reject('there is no user matches this email');
                }else{
                    bcrypt.compare(password,user.password).then(function(same){
                        if(!same){
                            mongoose.disconnect();
                            reject('password is incorrect');
                        }else{
                            //create session => [ npm install express-session connect-mongodb-session ]
                            mongoose.disconnect();
                            resolve(user._id);
                        }
                    });
                }
            }).catch(err =>{
                mongoose.disconnect();
                reject(err);
            });
        });
    });
}