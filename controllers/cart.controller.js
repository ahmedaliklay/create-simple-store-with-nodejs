const cartModel = require("../models/cart.model");

const validationResult = require('express-validator').validationResult;

exports.getCart = function(req,res,next){
    cartModel.getItemByUser(req.session.userId).then(function(items){
        res.render('cart',{
            items: items,
            isUser: true,
            pageTitle : 'Cart'
        });
    }).catch((err) => {
        console.log(err);
        res.redirect('/error');
    });
}

exports.postCart = (req,res,next) => {
    if(validationResult(req).isEmpty()){
        cartModel.addNewItem({
            name: req.body.name,
            price: req.body.price,
            amount: req.body.amount,
            productId: req.body.productId,
            userId: req.session.userId,
            timesTamp: Date.now()
            // name price productId amount image userId timesTamp
        }).then(() => {
            res.redirect('/cart');
        }).catch(err=>{
            console.log(err);
            res.redirect('/error');
        });
    }else{
        req.flash('CartValidationErrors',validationResult(req).array());
        res.redirect(req.body.redirectTo);
    }
};

exports.postSave = function(req,res,next){
    if(validationResult(req).isEmpty()){
    //console.log('!!!!!!!!!!! ' + req.body.cartId+ '!!!!!!!!!!!!!' + req.body.amount);
        cartModel.editItem(req.body.cartId, {
                amount: req.body.amount,
                timesTamp: Date.now()
            }).then(() => {
                res.redirect('/cart');
            }).catch((err)=> {
                console.log(err);
                res.redirect('/error');
            });
    }else{
        req.flash('CartValidationErrors',validationResult(req).array());
        res.redirect('/cart');
    }
}

exports.postDelete = function(req,res,next){
    if(validationResult(req).isEmpty()){
    cartModel.deleteItem(req.body.cartId).then(function(){
        res.redirect('/cart');
    }).catch(err=> console.log(err));
    }else{
        req.flash('CartValidationErrors',validationResult(req).array());
        res.redirect('/cart');
    }
}