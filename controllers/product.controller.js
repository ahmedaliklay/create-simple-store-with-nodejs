const productsModel = require('../models/products.model');

exports.getProduct = function(req,res,next){
    productsModel.getFirstProduct().then(function(product){
       
        res.render('product',{
            product: product,
            isUser: req.session.userId,
            pageTitle : 'product'
        });
       
    });
}
//name in  product route
exports.getProductById = function (req,res){
    
    let id = req.params.id;
  
        productsModel.getProductById(id).then(product => {
            res.render('product',{
                product: product,
                isUser: req.session.userId,
                pageTitle : 'Product'
            });
        });
   
}