const addModule = require('../models/products.model');

const validationResult = require('express-validator').validationResult;

exports.getAdd = function(req,res,next){
  
    res.render('add-product',{
        isUser: true,
        pageTitle : 'add-product'

    });

}

exports.postAdd = function(req,res,next){
    // console.log(req.file);
    // console.log(req.file.filename);
    // console.log(req.body);
    //console.log(validationResult(req).array());
    if(validationResult(req).isEmpty()){
        addModule.addNewProduct({
            name: req.body.name,
            price: req.body.price,
            image: req.file.filename,
            description: req.body.description,
            categor: req.body.category
        }).then(()=>{
            res.redirect('/'); 
            //console.log(res)   
        }).catch((err) => {
            console.log(err);
            res.redirect('/error');
        });
    }else{
        req.flash('errorAdd',validationResult(req).array());
        res.redirect('/add')
    }


    
}
