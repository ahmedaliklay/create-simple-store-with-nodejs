const productsModel = require('../models/products.model');

exports.getHome = function (req,res,next){
    /*
        get products
        render index.ejs
        productsModel.getAllProducts().then(function(products){
            console.log(products);
            res.render('index',{
                products: products
            });
        });
        get category
        if category && category !== all
        filter
        else
        rander all 
    */
    let categor = req.query.categor;
    let productPromise;
    validCategory = ['clothes','phones'];//check if category is found
    if(categor && validCategory.includes(categor)){
        productPromise = productsModel.getProductsByCategory(categor);
        productPromise.then(products => {
            console.log(products);
            res.render('index',{
                products: products,
                isUser: req.session.userId,
                CartValidationErrors: req.flash('CartValidationErrors')[0],
                pageTitle : 'Home'
            });
        });
    }else{ // Get All Products
        productPromise = productsModel.getAllProducts(categor);
        productPromise.then(products => {
            console.log(products);
            res.render('index',{
                products: products,
                isUser: req.session.userId,
                CartValidationErrors: req.flash('CartValidationErrors')[0],
                pageTitle : 'Home'
            });
        });
    }  
///////////////////////
    /*
    if(categor && categor !== 'all'){
       
        productsModel.getAllProducts(categor).then(function(products){
            console.log(products);
            res.render('index',{
                products: products
            });
        });
    }else{ 
        productsModel.getProductsByCategory(categor).then(function(products){
            console.log(products);
            res.render('index',{
                products: products
            });
        });
    }
    */
}