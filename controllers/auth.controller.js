const authModel = require('../models/auth.model');

const validationResult = require('express-validator').validationResult;
///////////////////////

exports.getSignup =  function(req,res,next){
    res.render('signup',{
        authError: req.flash('authError')[0],
        validationErrors: req.flash('validationErrors'),
        isUser: req.session.userId,
        pageTitle : 'SignIn'
    });
}
exports.postSignup =  function(req,res,next){
    //return console.log(validationResult(req).array());
   // res.render('signup');
   if(validationResult(req).isEmpty()){
        authModel.createNewUser(req.body.username, req.body.email, req.body.password)
        .then(function(){
            res.redirect('/login');
        }).catch(function(err){
            console.log(err);
            res.redirect('/signup');
        });// End Catch
   }else{
       req.flash('validationErrors',validationResult(req).array());
       res.redirect('/signup');
   }
   
}

exports.getLogin =  function(req,res,next){
    res.render('login',{
        authError: req.flash('authError')[0],
        validationLoginErrors: req.flash('validationLoginErrors'),
        isUser: req.session.userId,
        pageTitle : 'LogIn'
    });
}
exports.postLogin = function(req,res,next){
    //return console.log(validationResult(req).array());
    if(validationResult(req).isEmpty()){
        authModel.login(req.body.email, req.body.password)
        .then(function(id){
            req.session.userId = id;
            res.redirect("/");
        }).catch(function(err){
            console.log(err);
            //create validation with flashSession[npm install --save connect-flash]
            req.flash('authError',err);
            res.redirect("login");
        });
    }else{
        req.flash('validationLoginErrors',validationResult(req).array());
        res.redirect('/login');
    }
    
}
exports.logout = function(req,res,next){
    req.session.destroy(function(){
        res.redirect('/');
    });
}